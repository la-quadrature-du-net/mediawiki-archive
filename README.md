# MediaWiki Archive

Ceci est l'archive du MediaWiki de la Quadrature

This is an archive of La Quadrature's Mediawiki

---

There is two archives, one created with https://git.kaki87.net/KaKi87/mediawiki-export, and one create via httrack. 

You can download the whole archive and explore it with any web server, for example with `python -m http.server`

Il existe deux archives, une faite avec https://git.kaki87.net/KaKi87/mediawiki-export, et une faite avec httrack. 

Vous pouvez télécharger toute l'archive et explorer les archives avec un serveur web, comme par exemple avec `python -m http.server`